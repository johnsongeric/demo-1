
#include <conio.h> //so we can call _getch()
#include <iostream> //so we can use cout

using namespace std;

int main() // entry
{
	const float PI = 3.1415926f; // immutable

	int i = 3;
	double rainbow = 2.525;
	float f = 2.34f;
	
	bool b = true;
	char c = 'h';


	f = (float)rainbow; //casting double to float

	// You NEED to know
	// Arithmetic  / Assignment 
	// +, -, /, *, %, =, +=, -=, /=, *=, %=, ++, --
	//Logical operators, Relational
	// &&, ||, !, <, <=, >, >=, ==, !=

	// Nerds need to know
	// Bitwise
	// &, | , << , >> , ~, ^)
	// Ternary
	// (? : )



	i = 1;
	i += 10;
	i++;
	cout << i;

	//cout << "Input: ";
	//cin >> c;

	cout << "hi\n" << " \"how are you? " << c;

	_getch();
	return 0;
}